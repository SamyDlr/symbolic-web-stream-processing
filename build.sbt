name := "td1"

version := "0.1"

scalaVersion := "2.13.8"

libraryDependencies += "org.apache.jena" % "jena-core" % "4.3.2"
libraryDependencies += "org.apache.jena" % "apache-jena-libs" % "4.3.2"
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30"
libraryDependencies += "com.github.javafaker" % "javafaker" % "1.0.2"
libraryDependencies += "org.apache.kafka" %% "kafka-streams-scala" % "3.1.0"
dependencyOverrides += "org.apache.kafka" % "kafka-clients" % "3.1.0"
libraryDependencies += "io.confluent" % "kafka-streams-avro-serde" % "7.0.1"
//libraryDependencies += "com.twitter" %% "bijection-core" % "0.9.7"

resolvers += "Confluent" at "https://packages.confluent.io/maven" // to download kafka-streams-avro-serde

