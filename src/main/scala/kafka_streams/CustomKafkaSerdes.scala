package kafka_streams

import kafka_streams.records.{AnonVaccSideEffReport, AnonWithAgeSideEffReport, VaccinatedPersonAge, VaccineSideEffReport}
import org.apache.avro.Schema
import org.apache.avro.message.{BinaryMessageDecoder, BinaryMessageEncoder}
import org.apache.avro.specific.SpecificRecordBase
import org.apache.kafka.common.serialization.Serdes._
import org.apache.kafka.common.serialization.{Serde, Serdes}

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.util.logging.Logger

object CustomKafkaSerdes {
  class AvroSerde[T <: SpecificRecordBase](private val binaryMessageEncoder: BinaryMessageEncoder[T], private val binaryMessageDecoder: BinaryMessageDecoder[T], private val schema: Schema)
    extends Serdes.WrapperSerde[T]((_: String, data: T) => {
      binaryMessageEncoder.encode(data).array
    }, (_: String, data: Array[Byte]) => {
      binaryMessageDecoder.decode(data)
    }) {
    binaryMessageDecoder.addSchema(schema)
  }

  class VacineSideEffReportSerde extends AvroSerde[VaccineSideEffReport](VaccineSideEffReport.getEncoder, VaccineSideEffReport.getDecoder, VaccineSideEffReport.SCHEMA$)

  class AnonVaccSideEffReportSerde extends AvroSerde[AnonVaccSideEffReport](AnonVaccSideEffReport.getEncoder, AnonVaccSideEffReport.getDecoder, AnonVaccSideEffReport.SCHEMA$)

  class VaccinatedPersonAgeSerde extends AvroSerde[VaccinatedPersonAge](VaccinatedPersonAge.getEncoder, VaccinatedPersonAge.getDecoder, VaccinatedPersonAge.SCHEMA$)

  class AnonSideEffWithAgeSerde extends AvroSerde[AnonWithAgeSideEffReport](AnonWithAgeSideEffReport.getEncoder, AnonWithAgeSideEffReport.getDecoder, AnonWithAgeSideEffReport.SCHEMA$)

  implicit def vaccineSideEffSerde: Serde[VaccineSideEffReport] = new VacineSideEffReportSerde

  implicit def anonVaccSideEffSerde: Serde[AnonVaccSideEffReport] = new AnonVaccSideEffReportSerde

  implicit def vaccinatedPersonAgeSerde: Serde[VaccinatedPersonAge] = new VaccinatedPersonAgeSerde

  implicit def anonSideEffWithAgeSerde: Serde[AnonWithAgeSideEffReport] = new AnonSideEffWithAgeSerde

  implicit def compositeAgeVaccSIDERKeySerde: Serde[(Short, Short, String)] = new WrapperSerde[(Short, Short, String)](
    (_, tuple) => {
      val bb = StandardCharsets.US_ASCII.encode(tuple._3)
      if (!bb.hasRemaining) Logger.getLogger("mien").severe("aaaaah")


      val arr = new Array[Byte](bb.remaining + 4)
      bb.get(arr, 0, bb.remaining)

      var s = tuple._1
      arr(arr.length - 4) = (s >> 8).toByte
      arr(arr.length - 3) = s.toByte
      s = tuple._2
      arr(arr.length - 2) = (s >> 8).toByte
      arr(arr.length - 1) = s.toByte
      arr
    }, (_, arr) =>
      (
        (arr(arr.length - 4) << 8 | arr(arr.length - 3)).toShort,
        (arr(arr.length - 2) << 8 | arr(arr.length - 1)).toShort,
        StandardCharsets.US_ASCII.decode(ByteBuffer.wrap(arr, 0, arr.length - 4)).toString
      )

  )
}
