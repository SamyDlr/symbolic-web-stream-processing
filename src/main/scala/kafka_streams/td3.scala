package kafka_streams

import kafka_streams.records.{AnonVaccSideEffReport, VaccineSideEffReport}
import org.apache.kafka.streams.kstream.{Transformer, TransformerSupplier}
import org.apache.kafka.streams.state.{KeyValueStore, StoreBuilder, Stores}
import org.apache.kafka.streams.{KeyValue, processor}

import java.util
import java.util.Collections

object td3 {

  // TD3.Q2 VaccineSideEffReport -> AnonVaccSideEffReport
  val anonymizeReportsMapper: VaccineSideEffReport => AnonVaccSideEffReport = value => {
    AnonVaccSideEffReport.newBuilder()
      .setDate(value.getDate)
      .setVaccineName(value.getVaccineName)
      .setSideEffect(value.getSideEffect)
      .setSideEffectSIDER(value.getSideEffectSIDER)
      .setId(value.getId)
      .build()
  }

  // TD3.Q4
  val anonRepsGrpBySIDER: (Short, AnonVaccSideEffReport) => String = (k, report) => report.getSideEffectSIDER.toString


  private class CountByEffectTransformer(storeName: String) extends Transformer[Long, AnonVaccSideEffReport, KeyValue[String, Long]] {
    private var store: KeyValueStore[String, Long] = null
    private var context: processor.ProcessorContext = null
    private var name: String = null

    override def init(context: processor.ProcessorContext): Unit = {
      this.context = context
      store = context.getStateStore(storeName) // + context)
    }

    override def transform(key: Long, value: AnonVaccSideEffReport): KeyValue[String, Long] = {
      if (name == null) this.name = context.topic() + "-" + context.partition()
      val SIDER = value.getSideEffectSIDER.toString
      val count = store.get(SIDER) + 1
      store.put(SIDER, count)
      new KeyValue(SIDER, count)
    }

    override def close(): Unit = {
      val sb = new StringBuilder
      sb.append("Counter on ").append(name).append(" met:\n")
      var total: Long = 0
      store.all().forEachRemaining(kv => {
        sb.append("\t").append(kv.key).append(": ").append(kv.value).append('\n');
        total += kv.value
      })
      println(sb.append("Total counted reports: ").append(total).append("\n\n").toString)
    }
  }

  import org.apache.kafka.streams.scala.serialization.Serdes._

  abstract class CountTransformerSupplier(store: String) extends TransformerSupplier[Long, AnonVaccSideEffReport, KeyValue[String, Long]] {
    override def get(): Transformer[Long, AnonVaccSideEffReport, KeyValue[String, Long]] = {
      new CountByEffectTransformer(store)
    }

    override def stores(): util.Set[StoreBuilder[_]] =
      Collections.singleton(Stores.keyValueStoreBuilder(
        Stores.inMemoryKeyValueStore(store),
        stringSerde,
        longSerde
      ))
  }


  // TD3.Q5
  // We build our keys from [0; vaccines.size[, and have vaccines.size partitions ; so we don't manage keys outside that range (which we would ideally do)
  def byVaccinePartitioner(t: String, k: Long, value: AnonVaccSideEffReport, parts: Int): Int = {
    val i = value.getVaccineName.toString
    (i match {
      case "Pfizer" => 0
      case "Moderna" => 1
      case "Astrazeneca" => 2
      case "SpoutnikV" => 3
      case "CanSinoBio" => 4
    })

  }
}
