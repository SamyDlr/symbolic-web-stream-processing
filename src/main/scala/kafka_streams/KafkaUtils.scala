package kafka_streams

import org.apache.kafka.clients.admin.{AdminClient, AdminClientConfig, NewTopic}

import java.util.Properties
import scala.jdk.CollectionConverters._
import scala.sys.error
import scala.util.{Failure, Success, Using}

object KafkaUtils {

  def createTopics(t: Array[(String, Int)], host: String): Unit = {
    Using(AdminClient.create({
      val p = new Properties()
      p.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, s"$host:9092")
      p
    })) {
      admin =>
        val creation = admin.createTopics(t.map(e => new NewTopic(e._1, if (e._2 < 1) 1 else e._2, 1.toShort)).toList.asJava)
        creation.all.get()
    } match {
      case Failure(exception) => error("Couldn't create topic\r\n" + exception.getMessage); exception.printStackTrace(); System.exit(-1)
      case Success(v) =>
    }
  }
}
