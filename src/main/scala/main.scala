import com.github.javafaker.Faker
import jdk.internal.org.jline.utils.Log.error
import kafka_streams.KafkaUtils.createTopics
import kafka_streams.records.{AnonVaccSideEffReport, AnonWithAgeSideEffReport, VaccinatedPersonAge, VaccineSideEffReport}
import kafka_streams.td3
import kafka_streams.td3.{CountTransformerSupplier, byVaccinePartitioner}
import main.sideEffects
import org.apache.jena.atlas.lib.DateTimeUtils
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.ontology.{OntModel, OntModelSpec}
import org.apache.jena.rdf.model.impl.ResIteratorImpl
import org.apache.jena.rdf.model.{Model, ModelFactory, Property, Resource}
import org.apache.jena.util.iterator.ExtendedIterator
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.Serializer
import org.apache.kafka.streams.kstream.{Named, TimeWindows}
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.{Consumed, Materialized, Produced}
import org.apache.kafka.streams.scala.serialization.Serdes._
import org.apache.kafka.streams.state.{QueryableStoreTypes, ReadOnlyWindowStore, Stores}
import org.apache.kafka.streams.{KafkaStreams, StoreQueryParameters, StreamsConfig}

import java.io.FileOutputStream
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{Duration, Instant, LocalDate, Period}
import java.util
import java.util.{Calendar, Properties, RandomAccess, Timer, TimerTask}
import scala.Console.println
import scala.collection.convert.ImplicitConversions.`iterator asScala`
import scala.collection.{immutable, mutable}
import scala.jdk.CollectionConverters._
import scala.sys.exit
import scala.util.{Random, Sorting}

object main {
  val KAFKA_HOST = "localhost"
  val UNIV_BENCH_NS = "http://swat.cse.lehigh.edu/onto/univ-bench.owl#"
  val CUSTOM_NS = "http://swat.cse.lehigh.edu/onto/lubm1-ext#"
  val VACCINES_NS = "http://lubm1.vaccines.med/Vaccine"
  val model: Model = ModelFactory.createDefaultModel()
  val typeP: Property = model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
  // -- All name strings accessed from this Map, no need to take care of caps
  val vaccinesNames = Map("pf" -> "Pfizer", "mo" -> "Moderna", "as" -> "Astrazeneca", "sp" -> "SpoutnikV", "ca" -> "CanSinoBio")
  val VACC_ID_BY_FULL_NAME: immutable.Map[String, Short] = {
    val map = new mutable.HashMap[String, Short]()
    var i: Short = -1
    for (vacc <- vaccinesNames.values.toList.sorted) {
      i = (i + 1).asInstanceOf[Short]
      map.put(vacc, i)
    }
    map.toMap
  }

  private val receivedInjectionP = model.createProperty(CUSTOM_NS + "receivedInjection")

  val sideEffects = Array(("Injection site pain", "C0151828"), ("fatigue", "C0015672"), ("headache", "C0018681"), ("Muscle pain", "C0231528"), ("chills", "C0085593"), ("Joint pain", "C0003862"), ("fever", "C0015967"), ("Injection site swelling", "C0151605"), ("Injection site redness", "C0852625"), ("Nausea", "C0027497"), ("Malaise", "C0231218"), ("Lymphadenopathy", "C0497156"), ("Injection site tenderness", "C0863083"))
  val olderPpl = Set(
    model.createResource(UNIV_BENCH_NS + "AssistantProfessor"),
    model.createResource(UNIV_BENCH_NS + "AssociateProfessor"),
    model.createResource(UNIV_BENCH_NS + "FullProfessor"),
    model.createResource(UNIV_BENCH_NS + "GraduateStudent"),
    model.createResource(UNIV_BENCH_NS + "Lecturer")
  )

  val VACCINATED_TOPIC = "vaccinated"
  val SIDE_EFF_TOPIC = "sideEffects"
  val COUNT_BY_SIDER_TOPIC = "bySIDERCount"
  val ANON_SIDE_EFF_TOPIC = "anonymousSideEffects"
  val SID_EFF_COUNT_TOPIC = "sideEffectsCountTopic"
  val EFF_PARTED_BY_VACC_TOPIC = "sideEffectsByVacc"
  val EFF_BY_AGE_BY_VACCINE = "sideEffectsByAgeByVacc"
  val EFF_COUNT_BY_VACC_TOPIC = "sideEffectsCountByVacc"

  def main(args: Array[String]): Unit = {

    // INIT CODE

    Config.parseOption(args.toList)
    Config.validate()

    // init - loading input file
    var ont: OntModel = null
    try {
      var m = ModelFactory.createDefaultModel()
      m.read(UNIV_BENCH_NS)
      ont = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF, m)
      model.read("file:./lubm1.ttl", null, "TTL")
      ont.addSubModel(model)
    } catch {
      case e: Exception =>
        println(e)
        System.exit(-1)
    }
    println("files loaded")

    // some more init - vaccines RDF properties by short name, probability arrays
    var r: Resource = null
    val nameP = model.getProperty(UNIV_BENCH_NS + "name")
    val vaccines = vaccinesNames.map(v => {
      r = model.createResource(VACCINES_NS + v._2)
      model.add(r, nameP, v._2, XSDDatatype.XSDstring)
      (v._1, r)
    })
    var i = 0
    var j = 0f
    val vaccIds = new Array[String](vaccines.size)
    val vaccR = new Array[Float](vaccines.size)
    Config.getVaccinesProp.foreach(v => {
      vaccIds(i) = v._1
      j += v._2
      vaccR(i) = j
      i += 1
    })



    //var producer: KafkaProducer[String, AnonVaccSideEffReport] = null
    import kafka_streams.CustomKafkaSerdes._

    // PART 2 - KAFKA
    // Most of the code for this part takes place here. We're preparing the pipeline for the data generated later on. by the PART 1
    val builder = new StreamsBuilder()

    builder.stream[Long, VaccineSideEffReport](SIDE_EFF_TOPIC)
      // TD3.Q2
      .mapValues(td3.anonymizeReportsMapper, Named.as("ReportsAnonymizer"))
      .to(ANON_SIDE_EFF_TOPIC)

    val anonymousReports = builder.stream[Long, AnonVaccSideEffReport](ANON_SIDE_EFF_TOPIC)
    val peopleAgeTable = builder.table[Long, Short](VACCINATED_TOPIC)(Consumed.`with`[Long, Short])
    val sideEffCounts = "SideEffectsCounterStore"

    // TD3.Q4
    anonymousReports.transform(new CountTransformerSupplier(sideEffCounts) {}, Named.as(sideEffCounts))

    // TD3.Q5
    val effCountByVacc = "EffectsCountsByVaccineStore"

    anonymousReports.to(EFF_PARTED_BY_VACC_TOPIC)(Produced.`with`(longSerde, anonVaccSideEffSerde).withStreamPartitioner(byVaccinePartitioner))
    builder.stream[Long, AnonVaccSideEffReport](EFF_PARTED_BY_VACC_TOPIC)
      .transform(new CountTransformerSupplier(effCountByVacc) {}, Named.as(effCountByVacc))


    // TD.Q6
    val maxDuration = Duration.between(Instant.parse("2020-12-27T00:00:00.00Z"), Instant.now).minusHours(1)
    val queryableStore = anonymousReports.join(peopleAgeTable)((report, s) => new AnonWithAgeSideEffReport(report.getDate, report.getVaccineName, report.getSideEffect, report.getSideEffectSIDER, report.getId, s))
      .groupBy((_, report) => (((Math.floorDiv(report.getAge, 10) + 1) * 10).toShort, VACC_ID_BY_FULL_NAME(report.getVaccineName.toString), report.getSideEffectSIDER.toString))
      .windowedBy(TimeWindows.ofSizeAndGrace(Duration.of(1, ChronoUnit.HOURS), maxDuration))
      .count()(Materialized.as[(Short, Short, String), Long](Stores.inMemoryWindowStore("windowedCountsQueryableStore", maxDuration.plusHours(1), Duration.of(1, ChronoUnit.HOURS), false)))


    println("start processing")
    //
    val undergraduateStudent = ont.getOntClass(UNIV_BENCH_NS + "UndergraduateStudent")
    val p = new Proj(model, vaccR, vaccIds, vaccines, p => model.contains(p, typeP, undergraduateStudent))

    println("populating...")

    val t1 = System.nanoTime()

    // Listing from the base model is way faster than the OntModel. The OntModel gets us the subclasses and the Set removes the duplicates
    val set = new util.HashSet[Resource]
    val classes = ont.getOntClass(UNIV_BENCH_NS + "Person").listSubClasses(false)
    classes.forEach(cl => model.listResourcesWithProperty(typeP, cl).forEach(p => set.add(p)))

    p.populatePeopleData(new ResIteratorImpl(set.iterator))

    try {
      val out = new FileOutputStream("out.ttl")
      model.write(out, "TTL")
    } catch {
      case e: Exception =>
        error("Couldn't write output file out.ttl")
        e.printStackTrace()
    }

    val vaccinatedPeople = model.listSubjectsWithProperty(receivedInjectionP).toList

    // SECOND PART : KAFKA

    createTopics(Array((VACCINATED_TOPIC, 1), (SIDE_EFF_TOPIC, 1), (ANON_SIDE_EFF_TOPIC, 1), (EFF_PARTED_BY_VACC_TOPIC, vaccines.size)), KAFKA_HOST)
    println("topics created")

    val props = new Properties
    props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, s"$KAFKA_HOST:9092")
    props.setProperty(ProducerConfig.ACKS_CONFIG, "all")
    props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
    p.produceVaccinatedAgeRecords(props, VACCINATED_TOPIC, vaccinatedPeople)
    val b = builder.build()
    println(b.describe().toString)

    var streams: KafkaStreams = null
    streams = new KafkaStreams(b, new StreamsConfig(Map(
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG -> s"$KAFKA_HOST:9092",
      StreamsConfig.APPLICATION_ID_CONFIG -> "StreamProcessingApp",
      ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG -> "true",
      ProducerConfig.ACKS_CONFIG -> "all"
    ).asJava))
    streams.start()

    val t = new Timer()

    t.schedule(new TimerTask {
      override def run(): Unit = {
        val encoder = VaccineSideEffReport.getEncoder
        val prod = new KafkaProducer[Long, VaccineSideEffReport](props, longSerde.serializer(), ((_, report) => encoder.encode(report).array()): Serializer[VaccineSideEffReport])
        p.produceSideEffects(prod, props, SIDE_EFF_TOPIC, 5_000, vaccinatedPeople)
      }
    }, 2_000)


    val store: ReadOnlyWindowStore[(Short, Short, String), Long] = streams.store(StoreQueryParameters.fromNameAndType("windowedCountsQueryableStore", QueryableStoreTypes.windowStore[(Short, Short, String), Long]()).enableStaleStores())

    sys.ShutdownHookThread {
      val names = vaccinesNames.values.toList.sorted
      val it = store.all()
      var i: Long = 0
      it.foreach(kv => {
        i += kv.value
        println("Window from " + kv.key.window().startTime() + " to " + kv.key.window().endTime() + ", age slice: " + kv.key.key()._1 + ", vaccine: " + names(kv.key.key()._2) + ", side effect: " + kv.key.key()._3 + "\t->\t" + kv.value)
      })
      println("sum of the counts in Windowed store = " + i)
      it.close()
      streams.close(Duration.of(10, ChronoUnit.SECONDS))
      streams.cleanUp()
    }
  }
}

class Proj(model: Model, vaccProps: Array[Float], vaccIds: Array[String], vaccines: Map[String, Resource], isYoung: Resource => Boolean) {

  import main.{CUSTOM_NS, UNIV_BENCH_NS}

  val faker = new Faker()
  val rnd = new Random

  private val idP = model.createProperty(CUSTOM_NS + "personId")
  private val birthdateP = model.createProperty(CUSTOM_NS + "birthDate")
  private val lNameP = model.createProperty(CUSTOM_NS + "lastName")
  private val fNameP = model.createProperty(CUSTOM_NS + "firstName")
  private val genderP = model.createProperty(CUSTOM_NS + "gender")
  private val zipCodeP = model.createProperty(CUSTOM_NS + "zipCode")
  private val receivedInjectionP = model.createProperty(CUSTOM_NS + "receivedInjection")
  private val injectedVaccineP = model.createProperty(CUSTOM_NS + "injectedVaccine")
  private val dateP = model.createProperty(CUSTOM_NS + "date")
  private val nameP = model.createProperty(UNIV_BENCH_NS + "name")

  // CODE BELOW RELATED TO PART 2 - KAFKA

  private class ComparableProducerRecord(private val t: String, private val p: Integer, private val time: Long, private val id: Long, private val report: VaccineSideEffReport)
    extends ProducerRecord[Long, VaccineSideEffReport](t, p, time, id, report) with Ordered[ComparableProducerRecord] {

    override def compare(that: ComparableProducerRecord): Int = (timestamp() - that.timestamp()).toInt
  }

  def produceSideEffects(producer: KafkaProducer[Long, VaccineSideEffReport], props: Properties, topic: String, reportCount: Int, vaccinatedPeople: util.List[Resource]): Unit = {
    var vaccinated: util.List[Resource] = null
    if (!vaccinatedPeople.isInstanceOf[RandomAccess]) {
      vaccinated = new util.ArrayList[Resource](vaccinated)
    } else vaccinated = vaccinatedPeople


    cal.setTimeInMillis(Instant.now().minus(1, ChronoUnit.DAYS).toEpochMilli)
    val oneDayAgo = cal.getTime // 1 hour before now, to avoid issues with the Windowing operation with test data

    var reports = new Array[ComparableProducerRecord](reportCount)
    var p: Resource = null
    var sideEffect: (String, String) = null
    var vaccine: String = null
    //var id: Long = 0
    for (i <- 0 until reportCount) {
      p = vaccinated.get(rnd.nextInt(vaccinated.size()))
      val report = new VaccineSideEffReport()
      cal.setTime(faker.date().between(oneDayAgo, now))
      //java.sql.Date.valueOf(LocalDate.parse(p.getPropertyResourceValue(receivedInjectionP).getProperty(dateP).getString, DateTimeFormatter.ISO_DATE)), now))          // <== If we wanted more realistic dates
      report.setDate(DateTimeUtils.calendarToXSDDateString(cal))
      report.setFirstName(p.getProperty(fNameP).getString)
      report.setLastName(p.getProperty(lNameP).getString)
      vaccine = p.getPropertyResourceValue(receivedInjectionP)
        .getPropertyResourceValue(injectedVaccineP).getProperty(nameP).getString
      report.setVaccineName(vaccine)
      sideEffect = sideEffects(rnd.nextInt(sideEffects.length))
      report.setSideEffect(sideEffect._1)
      report.setSideEffectSIDER(sideEffect._2)
      val id = p.getProperty(idP).getLong
      report.setId(id)
      reports(i) = new ComparableProducerRecord(topic, null, cal.getTimeInMillis, id, report)
    }

    Sorting.quickSort(reports)
    reports.foreach(rep => producer.send(rep))
    producer.flush()
  }

  def produceVaccinatedAgeRecords(props: Properties, topic: String, vaccinatedPeople: util.List[Resource]): Unit = {
    import org.apache.kafka.streams.scala.serialization.Serdes._
    val producer = new KafkaProducer[Long, Short](props, longSerde.serializer(), shortSerde.serializer())
    var record: VaccinatedPersonAge = null

    vaccinatedPeople.forEach(p => {

      record = VaccinatedPersonAge.newBuilder()
        .setAge(Period.between(LocalDate.parse(p.getProperty(birthdateP).getString, DateTimeFormatter.ISO_DATE), LocalDate.now()).getYears).build()

      producer.send(new ProducerRecord(topic, p.getProperty(idP).getLong, Period.between(LocalDate.parse(p.getProperty(birthdateP).getString, DateTimeFormatter.ISO_DATE), LocalDate.now()).getYears.toShort))
    })
    producer.flush()
  }

  // CODE BELOW RELATED TO PART 1 - JENA DATA POPULATING

  private val cal = Calendar.getInstance()

  private def populatePersonData(p: Resource, isYoung: Boolean): Unit = {
    cal.setTime(if (isYoung) faker.date().birthday(18, 21) else faker.date().birthday(21, 70))
    model.add(p, birthdateP, DateTimeUtils.calendarToXSDDateString(cal), XSDDatatype.XSDdate)
    model.add(p, lNameP, faker.name.lastName(), XSDDatatype.XSDstring)
    model.add(p, fNameP, faker.name.lastName(), XSDDatatype.XSDstring)
    model.add(p, genderP, "" + (rnd.nextFloat() >= Config.getWomenProp()), XSDDatatype.XSDboolean)
    model.add(p, zipCodeP, faker.address().zipCode())
  }

  private def weightedRandomVaccId(vaccProbs: Array[Float]): Int = {
    var r = rnd.nextFloat() * 100;
    for (k <- vaccProbs.indices) {
      if (r < vaccProbs(k)) {
        return k
      }
    }
    return vaccProbs.length - 1
  }

  {
    cal.clear();
    cal.set(2020, 12, 28)
  }
  private val firstVaccDate = cal.getTime
  private val now = java.util.Date.from({
    val i = java.time.Instant.now()
    i.minusSeconds(3_600)
    i
  })

  def populatePeopleData(persons: ExtendedIterator[_ <: Resource]): Unit = {
    var injection: Resource = null
    var i = 0L
    var t1 = System.nanoTime
    persons.forEach(p => {
      populatePersonData(p, isYoung.apply(p))
      i += 1
      model.addLiteral(p, idP, i)
      if ((rnd.nextFloat * 100) < Config.getVaccinatedPopProp) {
        injection = model.createResource()
        injection.addProperty(injectedVaccineP, vaccines(vaccIds(weightedRandomVaccId(vaccProps))))
        cal.setTime(faker.date().between(firstVaccDate, now))
        injection.addProperty(dateP, DateTimeUtils.calendarToXSDDateString(cal), XSDDatatype.XSDdate)
        model.add(p, receivedInjectionP, injection)
      }
    })
    println("time to generate data: " + ((System.nanoTime - t1) / 1e9d))
    t1 = System.nanoTime
    println("persons count: " + i + " (8_330 is normal for lubm1.ttl)")
  }
}

import scala.collection.mutable.ListBuffer

object Config {
  private val vaccinesName = Array(
    "pf",
    "mo",
    "as",
    "sp",
    "ca"
  )

  var isValidated = false

  private var menProp: Float = 50.0f
  private var womenProp: Float = 50.0f
  private var vaccinatedPopProp: Float = 50.0f

  private val vaccinesProp = {
    val x = collection.mutable.Map[String, Float]()
    vaccinesName.foreach(y => x(y) = 100f / vaccinesName.length)
    x
  }
  type VaccinesProp = collection.mutable.Map[String, Float]

  def getMenProp(): Float = {
    if (!isValidated) throw new IllegalStateException
    else menProp
  }

  def getWomenProp(): Float = {
    if (!isValidated) throw new IllegalStateException
    else womenProp
  }

  def getVaccinatedPopProp: Float = {
    if (!isValidated) throw new IllegalStateException
    else vaccinatedPopProp
  }

  def getVaccinesProp: VaccinesProp = {
    if (!isValidated) throw new IllegalStateException
    else vaccinesProp
  }

  def setMenProp(menProp: Float): Unit = {
    if (isValidated) throw new Exception("Config already validated")
    Config.menProp = menProp
    Config.womenProp = 100f - menProp
  }

  def setWomenProp(womenProp: Float): Unit = {
    if (isValidated) throw new Exception("Config already validated")
    Config.womenProp = womenProp
    Config.menProp = 100f - womenProp
  }

  def setVaccinatedPopProp(vaccinatedPopProp: Float) = {
    if (isValidated) throw new Exception("Config already validated")
    Config.vaccinatedPopProp = vaccinatedPopProp
  }

  def setVaccinesProp(vaccinesPropStr: String): Unit = {
    if (isValidated) throw new Exception("Config already validated")
    val vaccinesPropLst = vaccinesPropStr.replace("\"", "").split(";").map(x => x.split(":")).toList
    val parsedNames = new ListBuffer[String]()
    var propsCount = 0f

    def nextVaccine(list: List[Array[String]]): Unit = {
      list match {
        case Array(name: String, prop: String) :: tail if vaccinesName.contains(name) =>
          Config.vaccinesProp(name) = prop.toFloat
          propsCount += prop.toFloat
          parsedNames += name
          nextVaccine(tail)
        case _ :: _ =>
          throw new Exception("Error parsing vaccines")
        case Nil => ()
      }
    }

    nextVaccine(vaccinesPropLst)
    if (propsCount > 100f) throw new Exception("Vaccines props must not be > 100")
    else if (propsCount < 100f && parsedNames.size == vaccinesName.size) throw new Exception("Vaccines props must be == 100")
    else if (propsCount <= 100f && parsedNames.size != vaccinesName.size) vaccinesName.foreach(x => if (!parsedNames.contains(x)) Config.vaccinesProp(x) = (100f - propsCount) / (vaccinesProp.size - parsedNames.size))
  }

  def parseOption(lst: List[String]): Unit = {
    lst match {
      case "-v" :: value :: tail =>
        setVaccinatedPopProp(value.toFloat)
        parseOption(tail)
      case "-m" :: value :: tail =>
        setMenProp(value.toFloat)
        parseOption(tail)
      case "-w" :: value :: tail =>
        setWomenProp(value.toFloat)
        parseOption(tail)
      case "-s" :: value :: tail =>
        setVaccinesProp(value)
        parseOption(tail)
      case _ :: _ =>
        println("Usage : -v <float> -m <float> -w <float> -s \"<vaccine_name>:<float>;<vaccine_name>:<float>...\" ")
        exit(1)
      case Nil => ()
    }
  }

  def validate(): Boolean = {
    if (womenProp > 0f && menProp > 0f && vaccinatedPopProp > 0f) {
      isValidated = true
    }
    isValidated
  }

}


object ConfigBuilder {
  val PFIZER = 0
  val MODERNA = 1
  val ASTRAZENECA = 2
  val SPOUTNIKV = 3
  val CANSINO = 4

  var women_perc = 48f
  var vaccinated_perc = 70f
  var vaccines_perc = mutable.Map[Int, Float]()

  private def isInvalidPercent(p: Float): Boolean = {
    return p < 0 || p > 100
  }

  def women_in_pop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    women_perc = percent
  }

  def vaccinated_in_pop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccinated_perc = percent
  }

  def pfizer_prop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccines_perc.update(PFIZER, percent)
  }

  def moderna_prop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccines_perc.update(MODERNA, percent)
  }

  def astrazeneca_prop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccines_perc.update(ASTRAZENECA, percent)
  }

  def spoutnik_prop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccines_perc.update(SPOUTNIKV, percent)
  }

  def cansino_prop(percent: Float): Unit = {
    if (isInvalidPercent(percent)) throw new IllegalArgumentException
    vaccines_perc.update(CANSINO, percent)
  }
}

class Config(val women_perc: Float, val vaccinated_perc: Float, vaccines_perc: Map[String, Float]) {
}